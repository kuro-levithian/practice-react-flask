import React from "react";
import { QueryClient, QueryClientProvider, useQuery} from 'react-query'
import User_Card from "../component/User_Card";

const queryClient = new QueryClient()

const fetchUser = async() => {
  const res = await fetch ("http://127.0.0.1:5000/all")
  return res.json()
}

const UserPage = () => {
  const { isLoading, isError, error, data } = useQuery("users",fetchUser)
  console.log(data)
  
  if (isLoading) {
    return <span>Loading...</span>
  }

  if(isError){
    return <span>Error: {error.message}</span>
  }

  return (
    <div>
      <h2>Users List</h2>
      <div>
        {data.map(user => (
            // eslint-disable-next-line react/jsx-pascal-case
            <User_Card key={user.id} user={user}/>
          ))}
      </div>
      <div>
        <a href="/login"><button>Login</button></a>
        <a href="/register"><button>Register</button></a>
        <a href="/posts"><button>Posts</button></a>
      </div>
    </div>
  );
};

const hof = (WrappedComponent) => {
  // Its job is to return a react component wrapping the baby component
  return (props) => (
      <QueryClientProvider client={queryClient}>
          <WrappedComponent {...props} />
      </QueryClientProvider>
  );
};

export default hof(UserPage);