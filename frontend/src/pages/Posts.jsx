import React,{useState} from 'react'
import { QueryClient, QueryClientProvider, useQuery} from 'react-query'
import NewPost from '../component/NewPost';
import PostList from '../component/PostList';


const fetchPosts = async() => {
    const res = await fetch ("http://127.0.0.1:5000/get_all_posts");
    return res.json();
}


const queryClient = new QueryClient()

const Posts = () => {
    const [editedPost, setEditedPost] = useState(null)

    const { isLoading, isError, data, error, refetch} = useQuery('posts', fetchPosts)

    if (isLoading) {
      return <span>Loading...</span>
    }

    if(isError){
      return <span>Error: {error.message}</span>
    }


    
    return (
      <div>
        <h2>Post</h2>
        <PostList data={data} refetch={refetch}/>
        <NewPost/>
     </div>
    )
}

const hof = (WrappedComponent) => {
  // Its job is to return a react component wrapping the baby component
  return (props) => (
      <QueryClientProvider client={queryClient}>
          <WrappedComponent {...props} />
      </QueryClientProvider>
  );
};

export default hof(Posts)