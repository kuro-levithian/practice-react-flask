import React from 'react'


const PostList = (props) => {

  const {refetch} = props

  const deleteAPi = (id) => {
    return fetch (`http://127.0.0.1:5000/delete_post/${id}/`,{
      "method":"DELETE",
      headers: {
        "Content-type":"application/json"
      },
    })
  }


  const deletePost = (data) => {
    deleteAPi(data.id).then(src =>  refetch?.())

  }

  return (
    <>
      {props.data && props.data.map(post => {
        return(
        <div className="card">
          <h3>{ post.title }</h3>
          <p>{post.detail}</p>
          <p>Create time : {post.date}</p>
          <div>
            {/* <button className='update-button' onClick={() => editPost(post)}>Update</button> */}
            <button className='delete-button' onClick={() => deletePost(post)}>Delete</button>
            </div>
        </div>
        )
      })}
    </>
  )
}

export default PostList