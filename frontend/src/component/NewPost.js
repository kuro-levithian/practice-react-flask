import React,{useState} from 'react'
import axios from 'axios';

const NewPost = () => {
    const [title, setTitle] = useState("")

    const [detail, setDetail] = useState("")

    const addPost = async(new_post) =>{
        const res = axios.post("http://127.0.0.1:5000/new_post",new_post)
        return res.json()
    }

    const handleOnClick = (e) =>{
      const new_post = {title, detail}
      addPost(new_post)
    }

  return (
    <div className='center'>
        <h3 style={{color:"black"}}>New Post</h3>
        <form>
            <div className="inputbox">
                <input type="text"  onChange={(e)=>setTitle(e.target.value)}/>
                <span>Title</span>
            </div>
            <div className="inputbox">
                <textarea type="text" onChange={(e)=>setDetail(e.target.value)}/>
                <span>Detail</span>
            </div>
            <div className="inputbox">
                <button type='submit' className='new-post-button' onClick={handleOnClick}>Submit</button>
            </div>
        </form>
    </div>
  )
}

export default NewPost